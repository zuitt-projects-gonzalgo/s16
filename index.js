function displayMsgToSelf() {
	console.log("Don't settle for less if you can do your best!")
}

let count = 1;

while(count <= 10) {
	displayMsgToSelf();
	count++;
}

//while loop
	//while loop will allow us to repeat an instruction as long as the condition is true
	/*
		Syntax:
		while(condition is true) {
			statement/instruction runs incrementation or decrementation
		}
	*/

let number = 5;
while(number <= 10) {
	console.log(`While ${number}`);
	number++;
};

// do-while loop
/*let number2 = Number(prompt("Give me a number"))

do{
	console.log(`Do While ${number2}`);
	number2 += 1;

} while(number2 < 10);*/

/*let counter = 1;
do {
	console.log(counter);
	counter++
} while(counter <= 21);*/

//for loop
	//it consists of three parts: initialization, condition, final expression
		//initialization - determines whrere the loop starts
		//condition - determines whien the loop stops
		//final expression - indicates how advance the loop (increment/decrement)
		/*
			syntax:
			for(initialization; condition; finalExpression) {
				statement
			}
		*/
/*for(let count = 0; count <= 20; count++) {
	console.log(count);
};
*/
let myString = "alex";
console.log(myString.length);
console.log(myString[myString.length - 1])

for(let x = 0; x < myString.length; x++) {
	console.log(myString[x]);
};

let myName = "Jetruson";
for(let i = 0; i < myName.length; i++) {
	if(
		myName[i].toLowerCase() == "a" ||
		myName[i].toLowerCase() == "e" ||
		myName[i].toLowerCase() == "i" ||
		myName[i].toLowerCase() == "o" ||
		myName[i].toLowerCase() == "u") {
			console.log(3);
	} else {
		console.log(myName[i]);
	}
}

//continue and break statements

for(let count = 0; count <= 20; count++) {
	if(count % 2 !== 0) {
		continue;
	} 
	console.log(`Continue and Break ${count}`)

	if(count > 10) {
		break;
	}
}

let name = "alexandro";

for(let i = 0; i < name.length; i++) {
	console.log(name[i]);

	if(name[i].toLowerCase() === "a") {
		/*console.log("Continue to the next iteration");*/
		continue;
	}

	if(name[i].toLowerCase() == "d") {
		break;
	}
}